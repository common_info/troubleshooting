Swagger PASS <https://192.168.72.62/api/pass/v1/docs/>

Swagger Globus <https://192.168.72.61/api/globus/v1/docs/>

Swagger OSS <https://192.168.72.63:8353/globus/v1/docs/>







### Тестовые зоны

Тестовая зона <https://192.168.72.61/#/auth/login>


|  | UI | Характеристики | IP | Тип, доступ к гипервизору | Ответственный |
| --- | --- | --- | --- | --- | --- |
| Globus | <https://globus.protei.ru> | ОС: Астра 2.12<br />CPU RAM HDD | ssh support@192.168.72.61 | Виртуалка, <https://192.168.72.46/ui/#/host/vms> | Давыдов |
| PASS | <https://pass.protei.ru> | ОС: Астра 2.12<br />CPU RAM HDD | ssh support@192.168.72.62 | Виртуалка, <https://192.168.72.46/ui/#/host/vms> | Давыдов |
| OSS | <https://oss.protei.ru> | ОС: Астра 1.7<br />CPU 2, RAM 4 GB, HDD 50 GB | ssh support@192.168.72.63 | Виртуалка, <https://ovirt.protei.ru/ovirt-engine/> | Солонченко |
| Monitoring<br />(grafana) | <https://192.168.109.74:3000/> | ОС: oracle linux | ssh support@192.168.109.74<br />A9w6daRW | Виртуалка, у админов | Солонченко |

# Тестовый openLDAP

Тестовый openLDAP развернут на зоне вместе с PASS (support@192.168.72.62)

1. Для подключения используем ADS(Apache Directory Studio)
   1. Настройки подключения
      ![](image.png)
   2. Логин `cn=admin,dc=pass,dc=ru`, пароль `password`

# Тестовая зона VNF

|  |  |
| --- | --- |
| bal1 | ssh support@172.30.247.163 |
| bal2 | ssh support@172.30.247.180 |
| mme1 | ssh support@172.30.247.245 |
| mme2 | ssh support@172.30.247.151 |
| mme3 | ssh support@172.30.247.212 |
| cli | ssh support@172.30.247.244 |
| monitoring | ssh support@192.168.109.74 A9w6daRW |
