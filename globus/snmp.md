По задаче GLOBUS-30:
[Репозиторий](https://git.protei.ru/rumyantsev/ktor-snmp-monitor)
[Проект на Jenkins](https://jenkins.protei.ru/job/lab/job/SNMP/job/SNMP-alert-converter/)

## Описание проекта

  Приложение предназначено для приема и обработки webhook moira, его конвертации в универсальный SNMP-trap версии SNMPv2c и отправки на SNMP-менеджер.

Приложение принимает `POST` запрос `/Alert`, разбирает тело json и отправляет snmp-трапы.
Содержимое запроса должно содержать поле trigger и массив events, на каждый элемент массива отправится один трап.


Пример валидного json:

```json
{
    "events": [
        {
            "metric": "WSMS-01.disk.used_percent.-boot",
            "old_state": "OK",
            "state": "WARN",
            "timestamp": 1635242853,
            "trigger_event": false,
            "value": 48.594352138480005
        }
    ],
    "trigger": {
        "description": "Description example",
        "id": "SMSW.WSMS-01.UsedDiskSpace",
        "name": "WSMS-01: High Disk Space Usage",
        "tags": [
            "SMSW",
            "System"
        ]
    }
}
```



## Требования

1\. Java 8
2\. В директории с исполняемым jar'ом должен быть файл `appConfig.json` (поставляется вместе с jar'ом в артефактах сборки)

## Настройки и логирование

Настройки в `appConfig.json`:
`host`, `port` - хост и порт для запуска
`snmpHost`, `snmpPort` - хост и порт snmp-менеджера, на который будут посылаться snmp-трапы
`snmpBaseOid` - базовый oid для отправляемых трапов



Логи пишутся в `${user.home}/logs/Snmp-Alert-Converter/`