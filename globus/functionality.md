1. Авторизация пользователей
2. Хранение, настройка пользователей (делается через LDAP)
3. Передача scope клиентам GLOBUS-A-106
4. Настройка ролей и привилегий под конкретное приложение (на приложении внедряющим PASS потребуется поддержка ролей и привилегий получаемых от PASS)(требует настройку групп в LDAP)
5. Смена пароля пользователем с учетом политик безопасности (**в работе**)

### В роадмапе

1. Журналирование событий авторизации через PASS в CEF-лог
2. Внедрение ролей в сам PASS
3. Получение списка пользователей с LDAP
4. Создание, удаление, редактирование пользователей через PASS в LDAP

PASS сервер и Глобус умеют вести аудит действий пользователя в формате cef-лога. Данная настройка включается в PASS и API-GW.

Пример лог файла:

```
CEF:1|Protei|GLOBUS-API-GW|0.1|refresh-token|refresh-token:FAILURE|5|dpt=8353 end=1701874448741 outcome=failure proto=TCP reason=400 request=https://globus.protei.ru/globus/v1/refresh-token requestMethod=POST spt= src=172.30.44.23 start=1701874448740
CEF:1|Protei|GLOBUS-API-GW|0.1|auth|auth:SUCCESS|5|dpt=8353 end=1701874448753 outcome=success proto=TCP reason=303 request=https://globus.protei.ru/globus/oauth2/v1/auth requestMethod=GET spt= src=172.30.44.23 start=1701874448753
CEF:1|Protei|GLOBUS-API-GW|0.1|start-session|start-session:SUCCESS|6|dpt=8353 end=1701874450623 outcome=success proto=TCP reason=200 request=https://globus.protei.ru/globus/v1/start-session requestMethod=POST spt= src=172.30.44.23 start=1701874448753
CEF:1|Protei|GLOBUS-API-GW|0.1|websocket/token/generate|websocket/token/generate:SUCCESS|5|dpt=8353 end=1701874450648 outcome=success proto=TCP reason=200 request=https://globus.protei.ru/globus/v1/websocket/token/generate requestMethod=POST spt= src=172.30.44.23 start=1701874450648
CEF:1|Protei|GLOBUS-API-GW|0.1|role/custom-list|role/custom-list:SUCCESS|5|dpt=8353 end=1701874450689 outcome=success proto=TCP reason=200 request=https://globus.protei.ru/globus/v1/role/custom-list requestMethod=POST spt= src=172.30.44.23 start=1701874450684
```

# Настройка

в config.yaml добавлена секция

```
    cef:
      device-vendor: "Protei"
      device-product: "API-GW"
      device-version: "0.1"
      default-severity: "5"
      commands-info:
        /oauth/v1/authorize:
          severity: "6"
      whitelist: []
      blacklist: []
```

настройка в logger.yaml:

```
    output: syslog
    enabled: false
    no-timestamp: true
    no-caller: true
    no-level: true
    level: debug
    mask: "CEF_MESSAGE_TAG"
```