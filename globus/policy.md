Общедоступная документация:
<https://pro-ldap.ru/tr/zytrax/ch6/ppolicy.html>
<https://linux.die.net/man/5/slapo-ppolicy>

Инструкция по настройке политик безопасности(**внутренний документ**) [Проверка политики паролей OpenLDAP.odt](%D0%9F%D1%80%D0%BE%D0%B2%D0%B5%D1%80%D0%BA%D0%B0%20%D0%BF%D0%BE%D0%BB%D0%B8%D1%82%D0%B8%D0%BA%D0%B8%20%D0%BF%D0%B0%D1%80%D0%BE%D0%BB%D0%B5%D0%B9%20OpenLDAP.odt)
проверочные дампы [ИБ-13.X Wireshark.zip](%D0%98%D0%91-13.X%20Wireshark.zip)

# Реализация политики паролей OpenLDAP



Инструкция по настройке политик безопасности выше. Основные этапы:

1. Загрузить модуль политики паролей [#Sozdanie-modulya-politik](https://youtrack.protei.ru/articles/GLOBUS-A-88/Politika-parolei#1-Sozdanie-modulya-politik)
2. Создать контейнер подразделения политики паролей
3. Создать политику паролей OpenLDAP для переопределения DN
4. Создать политику паролей OpenLDAP
   Определитесь какие политики нужно использовать для наших целей исходя из документации man slapo-ppolicy.

| **ИБ-13** | **Локальная парольная политика должна удовлетворять требованиям (пункты 13.Х ниже):** | **Используемые атрибуты** |
| --- | --- | --- |
| ИБ-13.1 | Устанавливается политика сложности пароля | PwdCheckQuality: 2 pwdMinLength: x   x — мин. длина пароля |
| ИБ-13.2 | Имеется возможность самостоятельной смены пароля пользователем | pwdAllowUserChange: TRUE |
| ИБ-13.3 | Обеспечивается ограничение неуспешных попыток входа | pwdLockout: TRUE pwdMaxFailure: 5<br /><br />сброс блокировки спустя указанное время (в сек)<br />pwdLockoutDuration: 900   постоянная блокировка pwdLockoutDuration: 0<br />или отсутствие данного параметра |
| ИБ-13.4 | Обеспечивается хранение истории паролей | pwdInHistory: x   x - кол-во выполненных подряд неудачных попыток подсоединения к каталогу |
| ИБ-13.5 | Устанавливается срок действия пароля | pwdMaxAge: x   x — кол-во секунд, после которого модифицированный пароль будет считаться устаревшим. |
| ИБ-13.6 | Применяется обязательная смена пароля при первом входе | pwdMustChange: TRUE |

# 1. Создание модуля политик

Сделал мини гайд по включению ppolicy на astralinux 1.7

(В моем случаи домен был `dc=protei`)

1. Зайти в файл **/etc/ldap/schema/ppolicy.ldif** и после строки

```
dn: cn=ppolicy,cn=schema,cn=config
```

добавить строку

```
changetype:   add
```

2. выполнить
   **ldapmodify -Y external -H ldapi:/// -f /etc/ldap/schema/ppolicy.ldif**

```
root@ldap-srv:/home/support# ldapmodify -Y external -H ldapi:/// -f /etc/ldap/schema/ppolicy.ldif
SASL/EXTERNAL authentication started
SASL username: gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth
SASL SSF: 0
adding new entry "cn=ppolicy,cn=schema,cn=config"
```

3. сделать файл **ppolicy-module.ldif** со следующим содержимым:

```
dn: cn=module{0},cn=config
changeType: modify
add: olcModuleLoad
olcModuleLoad: ppolicy
```

4. выполняем **ldapmodify -Y external -H ldapi:/// -f ppolicy-module.ldif**

```
root@ldap-srv:/home/support/ldap# ldapmodify -Y external -H ldapi:/// -f ppolicy-module.ldif
SASL/EXTERNAL authentication started
SASL username: gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth
SASL SSF: 0
modifying entry "cn=module{0},cn=config"
```

Проверяем

```
root@ldap-srv:/home/support/ldap# ldapsearch -Y external -H ldapi:/// -b cn=config "(objectClass=olcModuleList)" olcModuleLoad -LLL
SASL/EXTERNAL authentication started
SASL username: gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth
SASL SSF: 0
dn: cn=module{0},cn=config
olcModuleLoad: {0}back_mdb
olcModuleLoad: {1}memberof.la
olcModuleLoad: {2}ppolicy
```

5. Создаем **ppolicy-conf.ldif**

```
dn: olcOverlay=ppolicy,olcDatabase={1}mdb,cn=config
changetype:   add
objectClass: olcPpolicyConfig
olcOverlay: ppolicy
olcPPolicyDefault: cn=default,ou=pwpolicy,dc=protei
olcPPolicyUseLockout: FALSE
olcPPolicyHashCleartext: TRUE
```

6. Выполняем **ldapmodify -Y external -H ldapi:/// -f ppolicy-conf.ldif**

```
root@ldap-srv:/home/support/ldap# ldapmodify -Y external -H ldapi:/// -f ppolicy-conf.ldif 
SASL/EXTERNAL authentication started
SASL username: gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth
SASL SSF: 0
adding new entry "olcOverlay=ppolicy,olcDatabase={1}mdb,cn=config"
```

7. Создаем **ppolicy-defaut.ldif**

```
dn: ou=pwpolicy,dc=protei
changetype:   add
objectClass: organizationalUnit
objectClass: top
ou: pwpolicy

dn: cn=default,ou=pwpolicy,dc=protei
changetype:   add
objectClass: person
objectClass: pwdPolicyChecker
objectClass: pwdPolicy
cn: pwpolicy
cn: default
sn: pwpolicy
pwdAllowUserChange: TRUE
pwdAttribute: userPassword
pwdCheckQuality: 2
pwdLockout: TRUE
pwdMaxFailure: 2
pwdMustChange: TRUE
pwdReset: TRUE
pwdLockoutDuration: 45
pwdSafeModify: TRUE
pwdMinAge: 0
pwdInHistory: 1
pwdMinLength: 3
pwdFailureCountInterval: 0
pwdMaxAge: 0
```

8. Выполняем **ldapmodify -x -a -H ldap://ldap-srv.protei -D cn=admin,dc=protei -W -f ppolicy-defaut.ldif**

```
root@ldap-srv:/home/support/ldap# ldapmodify -x -a -H ldap://ldap-srv.protei -D cn=admin,dc=protei -W -f ppolicy-defaut.ldif 
Enter LDAP Password: 
adding new entry "ou=pwpolicy,dc=protei"

adding new entry "cn=default,ou=pwpolicy,dc=protei"
```