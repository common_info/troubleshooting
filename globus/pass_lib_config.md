PASS-lib устанавливается с помощью пакета protei-globus-api-gw. Настройка осуществляется посредством файла `/home/protei/Protei-Globus/API-GW/config/config.yaml` и управляется с помощью protei-daemon.

1. настройка порта доступа к сервису

```yaml
  api-gw-srv-endpoint:
    listen-addr: 0.0.0.0
    port: 8353
    use-tls: true
    server-cert-file: ./config/selfsigned.crt # Путь до файла с сертификатом, обязательно либо server-cert-file, либо server-key-file для сервера
    server-key-file:  # Путь до файла с ключами шифрования, обязательно либо server-cert-file, либо server-key-file для сервера
```

В данном случае доступ к библиотеке осуществляется через TCP порт 8253 с использованием HTTPS(TLS). При установке по умолчанию nginx автоматически перенаправляет запросы с globus.local на порт 8253.

2. Получение параметров доступа к PASS/OAuth2 серверу.
   Для получения параметров необходимо предоставить на PASS адрес страницы для переадресации с результатами авторизации (redirect_uri). Этот адрес состоит из двух частей: внешнее доменное имя библиотеки (параметр `api-gw-srv/default/pass-client/external-http-address` настраивается системными администраторами) и адрес страницы в самой библиотеке (параметр `api-gw-srv/default/pass-client/callback-path`). Для конфигурации по умолчанию redirect_uri равен `https://globus.local/globus/oauth2/v1/callback`.
   Полученная информация от PASS записывается в разделе `api-gw-srv/default/pass-client`.

```
 pass-client:
      auth-url: "https://pass.local/oauth/v1/authorize" # адрес для начала авторизации , получается на PASS
      client-id: "globus-e366-4d16-abd1-a63661d1a720" # client-id, присвоенный на PASS
      secret: "d28e93d6cfdbe4" # secret, присвоенный на PASS
      response-type: code # тип авторизации, разрешенный приложению на PASS
      token-url: "https://pass.local/oauth/v1/token" # адрес для обновления ключей
      revoke-url: "https://pass.local/oauth/v1/revoke" # адрес для отзыва ключей
      scopes: ["https://pass.protei.ru/auth/m2m"]
      session-auth-timeout: 3m # время ожидания подтверждения авторизации
      # redirect_uri собирается из <external-http-address>+<callback-path>
      # callback-path: "/globus/oauth2/v1/callback" -- убрали из конфига
      external-http-address: "https://globus.local"
```

! Необходимо перезапустить сервер после добавления настроек



### Настройка клиентской конфигурации для интеграции с PASS

Всё содержимое из config.yaml:api-gw-srv/default/pass-client вынесено в отдельный файл client.yaml
в config.yaml добавлен параметр pass-client-file-path, которым можно задать имя файла настроек. Если не задано (по умолчанию), то используется client.yaml в той же директории, что и рабочий config.yaml.