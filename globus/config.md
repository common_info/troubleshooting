После установки пакета все настройки сервиса PASS будут расположены согласно стандартной схеме размещения ПО Протей в `/home/protei/Protei-Globus/PASS/config`.
Настройки сервиса хранятся в файле `config.yaml`, используется формат YAML.

## 1. Настройка портов доступа

Для доступа к сервису авторизации используется OAuth2 протокол (HTTPS). Порт для доступа задается в секции `pass-srv-endpoint`. В настройке по умолчанию nginx осуществляет проброс запросов с порта 443 на порт 8352.
Пример конфигурации:

```yaml
  pass-srv-endpoint:
    listen-addr: 0.0.0.0
    port: 8352
    use-tls: true
    server-cert-file: ./config/selfsigned.crt # Путь до файла с сертификатом, обязательно либо server-cert-file, либо server-key-file для сервера
    server-key-file:  # Путь до файла с ключами шифрования, обязательно либо server-cert-file, либо server-key-file для сервера
```

В данном случае запросы будут приниматься на TCP порте 8352 с использованием шифрованного протокола HTTPS (TLS) и сертификатом для шифрования из файла `/home/protei/Protei-Globus/PASS/config.selfsigned.crt`.



## 2. Настройка доступа к LDAP

Настройка соединения к LDAP задаются к секции `ldap-endpoint`.

```yaml
  ldap-endpoint:
    service-access-url: 127.0.0.1
    port: 389
    use-tls: false    # true | false использоваться или нет SSL
    client-cert-file: # Путь до файла с сертификатом. При этом это поле не обязательно, даже если use-ssl = true
    client-key-file:  # Путь до файла с ключом. При этом это поле не обязательно, даже если use-ssl = true
    tls-server-name:  # Имя сервера в ответном сертификате для проверки. Необязательное, но повышает безопасность.
```



## 3. Настройка авторизации пользователей LDAP

Настройки для поиска и авторизации пользователя на LDAP хранятся в секции `pass-srv/default/ldap`.
Пример настройки:

```yaml
    ldap:
      endpoint: ldap-endpoint
      dn: ou=Users,dc=protei,dc=ru          # Корень LDAP дерева, под которым будут располагаться пользователи
      login-key: uid                           # Ключ по которому будет происходить поиск
#      admin-bind-dn: uid=support,ou=Users,dc=protei,dc=ru
#      admin-bind-password: elephant
```

В данном случае пользователи хранятся в LDAP дереве `ou=Users,dc=protei,dc=ru` и идентифицируются посредством аттрибута `uid`. Например, легальный DN для данной конфигурации: `uid=ivanov,ou=Users,dc=protei,dc=ru`.
Если политика LDAP требует, чтобы поиск осуществлял авторизованный пользователь, необходимо настроить параметры `admin-bind-dn`, `admin-bind-password`.

1. Определение групп, в которых состоит пользователь (необязательно, но необходимо для работы с ролями пользователей)
   Настройка поиска имен групп, в которых состоит пользователь, находится в секции `pass-srv/default/ldap/group-member-filter`.
   Пример настройки:

```yaml
     group-member-filter:
        group-base-dn: ou=Groups,dc=protei,dc=ru # Корень LDAP дерева, под которым будут располагаться группы
        direct-group-member:                     # Атрибуты, в которых указано название группы напрямую
          - memberOf
        # все остальные записи -- связь между атрибутом пользователя и атрибутом группы
        uid:
          - memberUid
        gidNumber:
          - gidNumber
        dn:
          - member
```

* Все группы должны располагаться в LDAP дереве, заданном параметром `group-base-dn`. Для данного примера допустимыми группами будут `ou=Administrators,ou=Groups,dc=protei,dc=ru`, `cn=Users,ou=Groups,dc=protei,dc=ru`.
* `direct-group-member` указывает LDAP-атрибуты пользователя, в которых хранится готовое название группы. Например, атрибут пользователя `memberOf=cn=group1,ou=Groups,dc=protei,dc=ru` указывает на принадлежность пользователя к группе `cn=group1,ou=Groups,dc=protei,dc=ru`.
* остальные параметры задают связь между атрибутом пользователя и атрибутом группы: какой атрибут пользователя и группы должны совпадать, чтобы принять решение о вхождении в группу. Согласно приложенному примеру: атрибут `uid` пользователя должен быть равен атрибуту `memberUid` группы или атрибут `gidNumber` пользователя был равен атрибуту `gidNumber` (в данном случае совпадающее название атрибутов) группы.
  Например:

```yaml
dn: cn=Group1,ou=Groups,dc=protei,dc=ru
objectClass: groupOfNames
member: uid=petrov,ou=Users,dc=protei,dc=ru

dn: cn=Group2,ou=Groups,dc=protei,dc=ru
objectClass: posixGroup
gidNumber: 123

dn: cn=Group3,ou=Groups,dc=protei,dc=ru
objectClass: posixGroup
gidNumber: 1000

dn: uid=petrov,ou=Users,dc=protei,dc=ru
objectClass: inetOrgPerson
gidNumber: 123
memberOf: cn=Group3,ou=Groups,dc=protei,dc=ru
```

Используя пример конфигурации выше, получим, что пользователь `petrov` состоит в группах `cn=Group1,ou=Groups,dc=protei,dc=ru` (через соответствие атрибута `dn` пользователя и `member` группы), `cn=Group2,ou=Groups,dc=protei,dc=ru` (через соответствие атрибутов `gidNumber`-`gidNumber`) и `cn=Group3,ou=Groups,dc=protei,dc=ru` (через прямое указание группы в `memberOf`)



## 4. Настройка клиентских приложений для авторизации пользователей (согласно OAuth2)

[The OAuth 2.0 Authorization Framework](https://www.rfc-editor.org/rfc/rfc6749)
**В конфигурации по умолчанию база данных клиентских приложений не сохраняется на диск и будет сброшена при перезапуске сервиса. Для сохранения настроек необходимо настроить сохранение базы данных клиентов на диск, место сохранения записывается в разделе `pass-srv/default/db-path`**

**Настройки клиентских приложений рекомендуется производить через приложение GLOBUS.**
В случае необходимости можно добавить конфигурацию клиентского приложения в `config.yaml` в раздел `pass-srv/default/clients`. Такие клиентские настройки будут применяться при каждом запуске сервиса (даже если они были удалены через приложение GLOBUS, эти настройки заново запишутся в базу после перезапуска).
Пример конфигурации:

```yaml
      "cid2":   # client_id, до 36 символов
        secret: "secret"
        response-type: code
        grant-type:
        - authorization_code
        - refresh_token
        redirect-uris: ["https://globus.local/globus/oauth2/v1/callback"]
        scopes: ["images"] # запрашиваемые права
        public: true                               # публичный сервер доступа, true - игнорируется secret
```

В данном примере клиентское приложение `cid2` будет авторизовать пользователей с использованием Authorization Code Grant.


PASS-lib устанавливается с помощью пакета protei-globus-api-gw. Настройка осуществляется посредством файла `/home/protei/Protei-Globus/API-GW/config/config.yaml` и управляется с помощью protei-daemon.

1. настройка порта доступа к сервису

```yaml
  api-gw-srv-endpoint:
    listen-addr: 0.0.0.0
    port: 8353
    use-tls: true
    server-cert-file: ./config/selfsigned.crt # Путь до файла с сертификатом, обязательно либо server-cert-file, либо server-key-file для сервера
    server-key-file:  # Путь до файла с ключами шифрования, обязательно либо server-cert-file, либо server-key-file для сервера
```

В данном случае доступ к библиотеке осуществляется через TCP порт 8253 с использованием HTTPS(TLS). При установке по умолчанию nginx автоматически перенаправляет запросы с globus.local на порт 8253.

2. Получение параметров доступа к PASS/OAuth2 серверу.
   Для получения параметров необходимо предоставить на PASS адрес страницы для переадресации с результатами авторизации (redirect_uri). Этот адрес состоит из двух частей: внешнее доменное имя библиотеки (параметр `api-gw-srv/default/pass-client/external-http-address` настраивается системными администраторами) и адрес страницы в самой библиотеке (параметр `api-gw-srv/default/pass-client/callback-path`). Для конфигурации по умолчанию redirect_uri равен `https://globus.local/globus/oauth2/v1/callback`.
   Полученная информация от PASS записывается в разделе `api-gw-srv/default/pass-client`.

```
 pass-client:
      auth-url: "https://pass.local/oauth/v1/authorize" # адрес для начала авторизации , получается на PASS
      client-id: "globus-e366-4d16-abd1-a63661d1a720" # client-id, присвоенный на PASS
      secret: "d28e93d6cfdbe4" # secret, присвоенный на PASS
      response-type: code # тип авторизации, разрешенный приложению на PASS
      token-url: "https://pass.local/oauth/v1/token" # адрес для обновления ключей
      revoke-url: "https://pass.local/oauth/v1/revoke" # адрес для отзыва ключей
      scopes: ["https://pass.protei.ru/auth/m2m"]
      session-auth-timeout: 3m # время ожидания подтверждения авторизации
      # redirect_uri собирается из <external-http-address>+<callback-path>
      # callback-path: "/globus/oauth2/v1/callback" -- убрали из конфига
      external-http-address: "https://globus.local"
```

! Необходимо перезапустить сервер после добавления настроек



### Настройка клиентской конфигурации для интеграции с PASS

Всё содержимое из config.yaml:api-gw-srv/default/pass-client вынесено в отдельный файл client.yaml
в config.yaml добавлен параметр pass-client-file-path, которым можно задать имя файла настроек. Если не задано (по умолчанию), то используется client.yaml в той же директории, что и рабочий config.yaml.



Scope - область данных или параметр передаваемый клиентскому приложению при авторизации пользователя.

Scope настраиваются в карточке клиента. В зависимости от настроенных скоупов клиент получает или не получает информацию касающуюся пользователя, который авторизуется через PASS. Данные передаются клиенту в ответом сообщении вместе с access_token.

1. login - скоуп отвечающий за получения логина пользователя
2. roles - скоуп отвечающие за выдачу клиенту ролей и привилегий пользователя
3. privileges - скоуп отвечающие за выдачу клиенту привилегий пользователя