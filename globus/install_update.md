В данном разделе описано как установить и настроить PASS сервер и Глобус - вэб интерфейс к PASS.



Общие требования к системе:

1. ОС - astra 1.7, ubuntu 22.04, RedOS 7.3.3 (начали проверку развертывания PASS)
2. 2 CPU, 4 GB RAM, 100 GB HDD - минимальные требования, в данный момент разрабатываем и тестируемся на них



Минимальные требования к ВМ мониторинга 4 CPU, 6 RAM, 50 диск, ОС последний Oracle Linux



Сторонние продукты:

| ПО | версия |
| --- | --- |
| postgres | 11.10 |
| nginx | 1.18.0 |
| LDAP | openLDAP (slapd 2.4.47) |
| SQlite |  |



#### Ссылки на образы ОС

Астра 2.12 <https://dl.astralinux.ru/astra/stable/2.12_x86-64/iso/>

РедОС <https://redos.red-soft.ru/product/downloads/>


# Описание пакетов

У нас в проекте есть несколько пакетов:

1. **protei-globus-api-gw** - это бэк для ГЛОБУС веба
2. **protei-globus-pass** - сам бэк, куда отправляет запросы protei-globus-api-gw и PASS-UI
3. **protei-globus-pass-nginx** - пакет, который настраивает nginx для работы с PASS-UI
4. **protei-globus-ui** - бэк веба ГЛОБУС
5. **protei-globus-ui-nginx** - пакет для настройки nginx для работы ГЛОБУС UI

Для разворачивания с 0 необходимо поставить 3 пакета:

1. protei-globus-pass-nginx (он зависит уже и от nginx и от protei-globus-pass)
2. protei-globus-ui-nginx (он зависит от protei-globus-ui)
3. protei-globus-api-gw

Все установится, настроится, создадутся все нужные сертификаты и будет доступно по двум доменным локальным именам (globus.local и pass.local)



# Установка PASS на одном сервере

1. Подключить репозитории  GLOBUS-A-101[: Подключение удаленных репозиториев](articles/GLOBUS-A-101/Podklyuchenie-udalennykh-repozitoriev)
2. Обновить пакеты `apt update`
3. Поставить пакет `apt install protei-globus-standalone`

# Установка PASS на двух серверах на примере астра линукс

 1. Подключаем репозитории GLOBUS-A-49, GLOBUS-A-101
 2. Обновить пакеты `apt update`
 3. Ставим пакеты на сервер PASS
    1. `apt-get install protei-globus-pass-nginx`
 4. Ставим пакеты на сервер Глобус
    1. `apt-get install protei-globus-ui-nginx`
    2. `apt-get install protei-globus-api-gw`
 5. При необходимости правим настройки nginx
    1. Для изменения имени сервиса поменять значение **`server_name`**` globus.local`(например на app.example.ru или ip-адрес) на собственное имя в файле по пути /etc/nginx/sites-enabled/globus-ui-nginx.conf
    2. Для подключения своих сертификатов замените значения переменных **`ssl_certificate`** и **`ssl_certificate_key`** в файле по пути /etc/nginx/sites-enabled/globus-ui-nginx.conf
 6. Запустить установленные сервисы
    1. На Глобусе` protei-daemon start protei-globus-api-gw`
    2. На PASS` protei-daemon start protei-globus-pass`
 7. Перезапустить nginx на обоих серверах
    1. `service nginx restart`
 8. В случае тестового развертывания добавить на локальной машине или dns сервере резолв имени указанном в server_name. ***Указанная ниже конфигурация подходит для общего тестового сервера***
    1. Для windows
       1. Добавить 2-е строчки в файл от администратора C:\\Windows\\System32\\drivers\\etc\\hosts
          1. `192.168.72.62	pass.local`
          2. `192.168.72.61	globus.local`
    2. Для linux
       1. Добавить 2-е строчки в файл от администратора /etc/hosts
          1. `192.168.72.62	pass.local`
          2. `192.168.72.61	globus.local`
    3. После добавления необходимо из браузера разрешить доступ к узлам
       1. Перейти по ссылкам `pass.local и globus.local`
 9. Для проверки успешного запуска сервисов
    1. `protei-daemon status service-name`
 10. Далее сервисы доступны через protei-daemon, используются стандартные команды на запуск, остановку.
     1. `protei-daemon start service-name`
     2. `protei-daemon stop service-name`




Для установки пакетов нужно:
1\. Прописать ключ от репозитория

Для deb пакетов

```
curl 'https://soup.protei.ru/public_key/download' | sudo apt-key add -
```

Для rpm пакетов

```
curl 'https://soup.protei.ru/public_key/download' -o /etc/pki/rpm-gpg/protei-soup
```



2\. Добавить сам репозиторий в систему
Для deb пакетов

```
echo 'deb https://soup.protei.ru/repos/apt pass_night/latest/ubuntu2004 main' > /etc/apt/sources.list.d/pass_night.list
```

Обнаружен язык C/C++

Для rpm пакетов

```
echo """[protei-pass_night]
name=protei soup pass_night
baseurl=https://soup.protei.ru/repos/yum/pass_night/latest/el8
enabled=1
gpgcheck=0
gpgkey=file:///etc/pki/rpm-gpg/protei-soup
""" > /etc/yum.repos.d/protei-pass_night.repo   
```

3\. Делаем apt-get update или yum update в зависимости от системы